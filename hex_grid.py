#!/usr/bin/env python

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append('/usr/share/inkscape/extensions')

# We will use the inkex module with the predefined Effect base class.
import inkex
# The simplestyle module provides functions for style parsing.
import simplestyle as sy

#for wiggling
import math
import random

class MakeHexGridEffect(inkex.Effect):
    """
    Creates a new layer with a hex grid using dimensions specified.
    """
    def __init__(self):
        """
        Constructor.
        """
        # Call the base class constructor.
        inkex.Effect.__init__(self)

        # Define integer options
        self.OptionParser.add_option('-l', '--length', action = 'store', type = 'int', dest = 'length', default = 5, help = 'length')
        self.OptionParser.add_option('-w', '--width', action = 'store', type = 'int', dest = 'width', default = 3, help = 'width')
        self.OptionParser.add_option('-g', '--height', action = 'store', type = 'int', dest = 'height', default = 4, help = 'height')
        self.OptionParser.add_option('-j', '--wiggle', action = 'store', type = 'int', dest = 'wiggle', default = 0, help = 'wiggle')

    def effect(self):
        """
        Effect behaviour.
        Overrides base class' method and draws a bunch of hexagons
        adds random wiggle to position
        """
        
        def wiggle(radius):
            norm = random.uniform(-1, 1)
            rads = math.pi * norm
            xoffset = math.cos(rads) * radius
            yoffset = math.sin(rads) * radius
            offtup = (xoffset, yoffset)
            return (offtup)

        length = self.options.length
        width = self.options.width
        height = self.options.height
        radius = self.options.wiggle

        svg = self.document.getroot()

        # Create a new layer.
        layer = inkex.etree.SubElement(svg, 'g')
        layer.set(inkex.addNS('label', 'inkscape'), 'hex grid layer')
        layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')     
        
        root_3 = 3.0 ** 0.5
        apothem = length * (root_3 / 2.0)
        halflength = length / 2.0
        style = {'stroke': '#cccccc', 'stroke-width': '1', 'fill': '#ff00ff'}
        
        for height_inc in range(height):
            for width_inc in range(width):
                if height_inc % 2 == 0:
            	     odd_row = 0
                else:
            	     odd_row = 1
                x_offset = (odd_row * apothem) + (width_inc * ( 2.0 * apothem)) 
                y_offset = height_inc * (length * (3.0/2.0))
                a_wiggle = wiggle(radius)
                xa = x_offset + a_wiggle[0]
                ya = y_offset + a_wiggle[1] + halflength
                b_wiggle = wiggle(radius)
                xb = x_offset + b_wiggle[0] + apothem
                yb = y_offset + b_wiggle[1] 
                c_wiggle = wiggle(radius)
                xc = x_offset + c_wiggle[0] + (apothem * 2)
                yc = y_offset + c_wiggle[1] + halflength
                d_wiggle = wiggle(radius)
                xd = x_offset + d_wiggle[0] + (apothem * 2)
                yd = y_offset + d_wiggle[1] + (length * (3.0/2.0))
                e_wiggle = wiggle(radius)
                xe = x_offset + e_wiggle[0] + apothem
                ye = y_offset + e_wiggle[1] + (length * 2)
                f_wiggle = wiggle(radius)
                xf = x_offset + f_wiggle[0]
                yf = y_offset + f_wiggle[1] + (length * (3.0/2.0))
                
                label = "hex w{} h{}".format(width_inc, height_inc)                
                
                path = "M {} {} L {} {} L {} {} L {} {} L {} {} L {} {} z".format(xa, ya, xb, yb, xc, yc, xd, yd, xe, ye, xf, yf)
                line_attribs = {'style' : sy.formatStyle(style), inkex.addNS('label','inkscape') : label, 'd' : path}
                line = inkex.etree.SubElement(svg, inkex.addNS('path','svg'), line_attribs )
        

# Create effect instance and apply it.
effect = MakeHexGridEffect()
effect.affect()
