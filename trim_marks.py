#!/usr/bin/env python

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append('/usr/share/inkscape/extensions')

# We will use the inkex module with the predefined Effect base class.
import inkex
# The simplestyle module provides functions for style parsing.
from simplestyle import *
import simpletransform as st

class MakeTrimMarksEffect(inkex.Effect):
    """
    Example Inkscape effect extension.
    Creates a new layer with trim marks around the selected object.
    """
    def __init__(self):
        """
        Constructor.
        Defines the "--offset" option of a script.
        """
        # Call the base class constructor.
        inkex.Effect.__init__(self)

        # Define integer option "--offset" with "-o" shortcut and default value 5.
        self.OptionParser.add_option('-o', '--offset', action = 'store',
          type = 'int', dest = 'offset', default = 5,
          help = 'offset')

    def effect(self):
        """
        Effect behaviour.
        Overrides base class' method and draws trim marks around selected object
        """
        # Get script's "--offset" option value.
        offset = self.options.offset
        length = 5
        base = 3
        off_len = offset + length
        
        if len(self.options.ids) == 0:
            inkex.errormsg(_("Please select an object."))
            exit()
        else:
            self.bbox = st.computeBBox(self.selected.values())


        # Get access to main SVG document element and get its dimensions.
        svg = self.document.getroot()

        # Create a new layer.
        layer = inkex.etree.SubElement(svg, 'g')
        layer.set(inkex.addNS('label', 'inkscape'), 'trim mark layer')
        layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
        
        tmxa = self.bbox[0] - off_len
        tmxb = self.bbox[0] - offset
        tmxc = self.bbox[0]
        tmxd = self.bbox[0] + base
        tmxe = self.bbox[1] - base
        tmxf = self.bbox[1]
        tmxg = self.bbox[1] + offset
        tmxh = self.bbox[1] + off_len
        
        tmya = self.bbox[2] - off_len
        tmyb = self.bbox[2] - offset
        tmyc = self.bbox[2]
        tmyd = self.bbox[2] + base
        tmye = self.bbox[3] - base
        tmyf = self.bbox[3]
        tmyg = self.bbox[3] + offset
        tmyh = self.bbox[3] + off_len       
        
        path_a = "M {} {} L {} {} L {} {} z".format(tmxc, tmya, tmxc, tmyb, tmxd, tmya)
        path_b = "M {} {} L {} {} L {} {} z".format(tmxe, tmya, tmxf, tmya, tmxf, tmyb)    
        path_c = "M {} {} L {} {} L {} {} z".format(tmxa, tmyc, tmxb, tmyc, tmxa, tmyd)    
        path_d = "M {} {} L {} {} L {} {} z".format(tmxg, tmyc, tmxh, tmyc, tmxh, tmyd)   
        path_e = "M {} {} L {} {} L {} {} z".format(tmxa, tmye, tmxb, tmyf, tmxa, tmyf)
        path_f = "M {} {} L {} {} L {} {} z".format(tmxg, tmyf, tmxh, tmye, tmxh, tmyf)    
        path_g = "M {} {} L {} {} L {} {} z".format(tmxc, tmyg, tmxd, tmyh, tmxc, tmyh)    
        path_h = "M {} {} L {} {} L {} {} z".format(tmxe, tmyh, tmxf, tmyg, tmxf, tmyh) 
        
        style = {'stroke': '#cccccc', 'stroke-width': '0', 'fill': '#ff00ff'}

        line_attribs_a = {'style' : formatStyle(style), inkex.addNS('label','inkscape') : 'path a', 'd' : path_a}
        line_attribs_b = {'style' : formatStyle(style), inkex.addNS('label','inkscape') : 'path b', 'd' : path_b}
        line_attribs_c = {'style' : formatStyle(style), inkex.addNS('label','inkscape') : 'path c', 'd' : path_c}
        line_attribs_d = {'style' : formatStyle(style), inkex.addNS('label','inkscape') : 'path d', 'd' : path_d}
        line_attribs_e = {'style' : formatStyle(style), inkex.addNS('label','inkscape') : 'path e', 'd' : path_e}
        line_attribs_f = {'style' : formatStyle(style), inkex.addNS('label','inkscape') : 'path f', 'd' : path_f}
        line_attribs_g = {'style' : formatStyle(style), inkex.addNS('label','inkscape') : 'path g', 'd' : path_g}
        line_attribs_h = {'style' : formatStyle(style), inkex.addNS('label','inkscape') : 'path h', 'd' : path_h}
        
        line_a = inkex.etree.SubElement(svg, inkex.addNS('path','svg'), line_attribs_a )
        line_b = inkex.etree.SubElement(svg, inkex.addNS('path','svg'), line_attribs_b )
        line_c = inkex.etree.SubElement(svg, inkex.addNS('path','svg'), line_attribs_c )
        line_d = inkex.etree.SubElement(svg, inkex.addNS('path','svg'), line_attribs_d )
        line_e = inkex.etree.SubElement(svg, inkex.addNS('path','svg'), line_attribs_e )
        line_f = inkex.etree.SubElement(svg, inkex.addNS('path','svg'), line_attribs_f )
        line_g = inkex.etree.SubElement(svg, inkex.addNS('path','svg'), line_attribs_g )
        line_h = inkex.etree.SubElement(svg, inkex.addNS('path','svg'), line_attribs_h )
        

# Create effect instance and apply it.
effect = MakeTrimMarksEffect()
effect.affect()